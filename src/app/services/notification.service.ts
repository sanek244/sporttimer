import { Injectable, OnDestroy } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ModelNotification } from '../models/ModelNotification';

//Показ сообщение в правом, верхнем углу
@Injectable({
  providedIn: 'root'
})
export class NotificationService implements OnDestroy {

  private _messages = new BehaviorSubject<ModelNotification[]>([]); //сообщения

  private _timerRemoveMessages: NodeJS.Timer|undefined; //интервал для удаления сообщений

  //для внешнего использования
  public messages$ = this._messages.asObservable();
  
  constructor() { 
    this.removeMessages = this.removeMessages.bind(this);

    this._timerRemoveMessages = undefined;
  }

  ngOnDestroy() {
    clearTimeout(this._timerRemoveMessages);
  }

  //set/get
  public set messages(newValue: ModelNotification[]){
    this._messages.next(newValue);
  }
  public get messages(): ModelNotification[]{
    return this._messages.getValue();
  }


  removeMessages(){
    this.messages = this.messages.filter(x => x.endTime > Date.now());
    this.setTimetout();
  }

  removeMessage(uid: string){
    this.messages = this.messages.filter(x => x.uid != uid);
    this.setTimetout();
  }

  setTimetout(){
    if(this.messages.length){
      this._timerRemoveMessages = setTimeout(this.removeMessages, this.messages.last().endTime - Date.now());
    }
    else{
      clearTimeout(this._timerRemoveMessages);
      this._timerRemoveMessages = undefined;
    }
  }

  addMessage(newMessage: ModelNotification){
    this.messages = this.messages.concat(newMessage);

    if(!this._timerRemoveMessages){
      this._timerRemoveMessages = setTimeout(this.removeMessages, newMessage.endTime - Date.now());
    }
  }
}
